import $storage from "../../util/localStorage.js";
import store from "../index.js";
import {MenuUtil} from "../../util/menuUtil.js";

/**
 * 定义状态变量
 */
const state = {
    // 侧边栏是否折叠：true-折叠，false-展开
    isCollapsed: $storage.get('isCollapsed') === 'true' || false,
    // 默认激活菜单
    defaultActiveMenuName: $storage.get('defaultActiveMenuName') || '',
    // 面包屑导航列表
    breadCrumbList: $storage.getJson('breadCrumbList') || [],
};

/**
 * 定义修改状态的同步方法
 * 通过 commit 调用 mutations 定义的方法
 */
const mutations = {
    SET_SIDEBAR_COLLAPSED(state) {
        // 取反：原来如果是折叠状态，那么就改为展开状态，反之亦然
        state.isCollapsed = !state.isCollapsed;
        // 保存到 localStorage 里面
        $storage.set('isCollapsed', state.isCollapsed);
    },
    UPDATE_BREAD_CRUMB_LIST(state, routeName) {
        console.log('更新', routeName)
        // 更新默认激活菜单名称
        state.defaultActiveMenuName = routeName;
        $storage.set('defaultActiveMenuName', state.defaultActiveMenuName);
        // 根据 routeName 获取对应的面包屑导航列表，找出所有上级菜单对象
        state.breadCrumbList = MenuUtil.findParentMenus(store.getters.menuList, routeName);
        $storage.setJson('breadCrumbList', state.breadCrumbList);
    }
};

/**
 * 定义修改状态的同步方法
 * 通过 dispatch 调用 actions 定义的方法
 */
const actions = {
    updateSidebarCollapsed({commit}) {
        commit('SET_SIDEBAR_COLLAPSED');
    },
};

// 导出相关的对象
export default {
    namespaced: true, state, mutations, actions
};