import {defineConfig} from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
// 引入SVG插件
import {createSvgIconsPlugin} from 'vite-plugin-svg-icons';

// 设置服务配置信息
export default defineConfig({
    plugins: [
        vue(),
        createSvgIconsPlugin({
            // vite-plugin-svg-icons 插件会读取指定文件夹下的所有svg图标
            iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg')],
            // 指定生成SVG的 symbolId 格式
            symbolId: 'icon-[name]',
        })
    ],
    server: {
        port: 3500,
        host: '0.0.0.0',
        // 启动服务时自动打开浏览器
        open: true,
        proxy: {
            // 配置代理，针对所有 /api 开头的请求，都将转发到 http://127.0.0.1:8080/api-v2 服务器
            '/api': {
                // 后端服务地址
                target: 'http://127.0.0.1:8888/api-v2',
                // 允许跨域，后端也需要同步设置跨域
                changeOrigin: true,
                // 路径重写，所有以 /api 开头的请求，其中 /api 部分都将被替换为空字符串
                // 举个例子：请求 /api/user 将会被代理到 http://127.0.0.1:8080/api-v2/user
                rewrite: (path) => path.replace(/^\/api/, '')
            }
        }
    },
    build: {
        outDir: 'dist', // 构建输出目录
        assetsDir: 'assets', // 静态资源目录
        minify: 'terser', // 压缩代码，可选'esbuild'或'terser'
        terserOptions: {
            compress: {
                drop_console: true // 生产环境去除 console 语句
            }
        }
    }
});