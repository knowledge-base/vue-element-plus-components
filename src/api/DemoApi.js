import $axios from "../util/request.js";

// 定义接口
export function getData() {
    // 这里写后端对应的接口地址
    return $axios.get('/api/getData')
}