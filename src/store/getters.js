/**
 * 导出状态变量
 */
export default {
    // 定义状态变量，并且从 login 模块中获取 tokenValue 的值
    // 当前的 tokenValue 发生改变时，这个状态变量也会跟着改变
    // 这就相当于 Vue 的计算属性
    token: state => state.login.tokenValue,
    menuList: state => state.login.menuList,
    isCollapsed: state => state.sidebar.isCollapsed,
    defaultActiveMenuName: state => state.sidebar.defaultActiveMenuName,
    breadCrumbList: state => state.sidebar.breadCrumbList,
};