/**
 * token认证工具类
 */
export const AuthUtil = {
    getToken: () => {
        return localStorage.getItem('token');
    }
};