/**
 * 模拟后端数据
 */
export const MockUtil = {
    /**
     * 用户菜单数据
     */
    getMenuData: (username) => {
        const menuList = [];
        if (username === 'admin') {
            menuList.push(
                {
                    path: '#',
                    name: 'System',
                    hidden: false,
                    meta: {
                        title: '系统管理', icon: 'Setting'
                    },
                    component: '#',
                    children: [
                        {
                            path: '/demo/pageOne',
                            name: 'pageOne',
                            meta: {
                                title: '页面01', icon: 'Position'
                            },
                            component: '/demo/PageA',
                        },
                        {
                            path: '/demo/pageTwo',
                            name: 'pageTwo',
                            meta: {
                                title: '页面02', icon: 'ElemeFilled'
                            },
                            component: '/demo/PageB',
                        },
                    ]
                });
        }
        menuList.push(
            {
                path: '#',
                name: 'Other',
                meta: {
                    title: '测试菜单', icon: 'Message'
                },
                component: '#',
                children: [
                    {
                        path: '/page-c',
                        name: 'pageC',
                        meta: {
                            title: 'PageC界面', icon: 'HelpFilled'
                        },
                        component: '/demo/PageC',
                    },
                    {
                        path: '#',
                        name: 'second-menu',
                        meta: {
                            title: '二级菜单', icon: 'Message'
                        },
                        component: '#',
                        children: [
                            {
                                path: '/page-d',
                                name: 'pageD',
                                meta: {
                                    title: 'PageD界面', icon: 'HelpFilled'
                                },
                                component: '/demo/PageD',
                            },
                        ]
                    }
                ]
            }
        );

        menuList.push(
            {
                path: '/page-e',
                name: 'pageE',
                meta: {
                    title: 'PageE界面', icon: 'HelpFilled'
                },
                component: '/demo/PageE',
            },
        );

        return menuList;
    }
};