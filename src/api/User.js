import request from "../util/request.js";

const API_PREFIX = '/api/v1/user';

// TODO 这里定义后端的登录接口
export function userLogin(data) {
    return request.post(`${API_PREFIX}/login`, data);
}