import {createApp} from 'vue';
import App from './App.vue';
// 引入路由
import router from './router';
import './router/permission.js';
// 引入Element Plus组件库
import ElementPlus from 'element-plus';
// 先导入element-plus的样式，再导入自定义的样式
import 'element-plus/dist/index.css';
// 先导入element-plus的样式，再导入自定义的样式
import './assets/styles/styles.css';
// 引入图标
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
// 引入Vuex插件
import store from "./store/index.js";
// 引入 font-awesome 图标库
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
// 按需引入要使用的图标，例如：我要使用 faHome 这个图标，那就需要引入这个图标
import {faHome,faUniversalAccess,faCompress,faExpand,faSync} from '@fortawesome/free-solid-svg-icons';
// 导入SVG图标注册脚本
import 'virtual:svg-icons-register';

// 创建Vue3实例对象
const app = createApp(App);
// 挂载路由之前，重新获取一下菜单数据
const initMenuList = () => {
    // 重新加载一下菜单数据，并将其添加到router里面，解决页面刷新白屏的问题
    store.dispatch('login/getUserMenuList');
};
initMenuList();
console.log('1、main.js中先挂载router')
// 使用路由
app.use(router);
// 使用Vuex插件
app.use(store);
// 注册图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
// 添加图标到库中
library.add(faHome,faUniversalAccess,faCompress,faExpand,faSync);
// 注册图标组件
app.component('font-awesome-icon', FontAwesomeIcon);
app.use(ElementPlus);
// 挂载Vue3实例到 #app 容器
app.mount('#app');
