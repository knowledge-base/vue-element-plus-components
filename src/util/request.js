import axios from 'axios';
import {ElMessage} from 'element-plus';

/**
 * 创建 axios 实例对象
 * <p>
 *     配置请求的根路径，超时时间，请求头等参数
 * </p>
 */
const $axios = axios.create({
    baseURL: '', // 设置后端接口的服务地址
    timeout: 3000, // 设置超时时间
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
});

/**
 * 请求拦截器
 * <p>
 *     每次请求都会执行拦截器，可以在请求头中添加 token 等参数
 * </p>
 */
$axios.interceptors.request.use(
    config => {
        // 可以在这里添加请求头，例如添加 token 等
        console.log('请求拦截参数：', config);
        return config;
    },
    error => {
        console.log('请求拦截器', error)
        return Promise.reject(error);
    }
);

/**
 * 响应拦截器
 * <p>
 *     响应拦截器，接口返回数据之后，会执行响应拦截器
 *     可以根据接口返回的数据，做相应的处理，例如：成功、失败、未登录、无权限等
 * </p>
 */
$axios.interceptors.response.use(
    response => {
        console.log('响应拦截器', response)
        const {code, message} = response.data;
        if (code === 200) {
            // 成功的处理逻辑
            return response.data;
        } else if (code === 401) {
            // 无权限的处理逻辑
            ElMessage.error('您无权进行此操作！');
            return Promise.reject(response.data);
        } else if (code === 403) {
            // 未登录的处理逻辑
            ElMessage.error('您尚未登录，请先登录！');
            return Promise.reject(response.data);
        } else {
            // 其他错误的处理逻辑
            ElMessage.error(message);
            return Promise.reject(response.data);
        }
    },
    error => {
        console.log('响应拦截器error:', error)
        // 统一的错误提示
        if (error === 'Cancel') {
            return Promise.reject(error);
        }
        let {message} = error;
        if (message === 'Network Error') {
            message = '网络连接异常！';
        } else if (message?.includes('timeout')) {
            message = '请求超时，请联系管理员！';
        } else if (message?.includes('Request failed with status code')) {
            message = '未知异常，请联系管理员！';
        }
        ElMessage.error(message);
        // 对响应错误做一些统一的处理
        return Promise.reject(error);
    }
);

// 导出axios实例对象
export default $axios;