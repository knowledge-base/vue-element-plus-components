/**
 * 路由权限控制
 */
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import router from "./index.js";
import {AuthUtil} from "../util/auth.js";
import store from "../store/index.js";
import $storage from "../util/localStorage.js";

/**
 * 进度条配置
 */
NProgress.configure({
    // 动画方式
    easing: 'ease',
    // 关闭旋转圈圈
    showSpinner: false,
});

/**
 * 路由白名单，这些路由不需要判断token是否存在
 */
const whiteUrl = ['/login', '/logout'];

/**
 * 路由前置守卫
 */
router.beforeEach((to, from, next) => {
    // 开启进度条
    NProgress.start();
    // 判断是否存在token信息
    if (AuthUtil.getToken()) {
        // 已经登录过，如果当前访问的是 login 登录界面，则直接跳转到后台首页面
        if (to.path === '/login') {
            // todo 这里根据实际应用程序跳转的来写
            next('/main');
        } else {
            // 更新面包屑导航信息、更新默认激活菜单名称
            store.commit('sidebar/UPDATE_BREAD_CRUMB_LIST', to.name);
            // 访问的不是登录界面，则从后端获取菜单数据
            store.dispatch('login/getUserMenuList').then((res) => {
                // console.log('获取菜单数据成功', res)
                // 菜单获取成功，则跳转对应页面
                next();
            }).catch((err) => {
                console.log('获取菜单数据失败', err)
                // 获取菜单数据失败，清空 localStorage，并且跳转到登录页
                $storage.clear();
                next('/login');
            });
        }
    } else {
        // 不存在token信息的情况下，判断当前路由是否在白名单里面
        if (whiteUrl.includes(to.path)) {
            // 在白名单里面，直接跳转到对应的界面
            next();
        } else {
            // 不在白名单里面，跳转到登录页
            next('/login');
        }
    }
});

/**
 * 路由后置守卫
 */
router.afterEach((to, from) => {
    // 关闭进度条
    NProgress.done();
    document.title = to.meta?.['title'] || '管理后台';
});