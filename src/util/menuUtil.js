/**
 * 菜单工具类
 */
export const MenuUtil = {
    // 递归查找所有父级菜单
    findParentMenus: (menuData, targetName, result = []) => {
        for (const item of menuData) {
            const hasChildren = item.children && item.children.length > 0;
            const current = {
                title: item.meta?.title,
                name: item.name,
                path: item.path,
                // 面包屑
                breadCrumbRouteName: hasChildren ? item.children[0].name : '',
                breadCrumbPath: hasChildren ? item.children[0].path : '',
                hasChildren
            };
            const newResult = [...result, current];
            if (item.name === targetName) {
                return newResult;
            }
            if (hasChildren) {
                // 递归处理子菜单
                const result = MenuUtil.findParentMenus(item.children, targetName, newResult);
                if (result && result.length > 0) {
                    return result;
                }
            }
        }
        return [];
    }
};