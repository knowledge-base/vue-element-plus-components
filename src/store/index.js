import {createStore} from 'vuex';
import getters from './getters';
import login from "./modules/login.js";
import sidebar from "./modules/sidebar.js";

/**
 * store对象
 */
const store = createStore({
    modules: {
        login,
        sidebar
    },
    getters
});
// 导出 store 对象
export default store;