/**
 * localStorage 工具类
 */
const $storage = {
    set: (key, value) => {
        localStorage.setItem(key, value);
    },
    setJson: (key, value) => {
        localStorage.setItem(key, JSON.stringify(value));
    },
    get: (key) => {
        return localStorage.getItem(key);
    },
    getJson: (key) => {
        return JSON.parse(localStorage.getItem(key));
    },
    remove: (key) => {
        localStorage.removeItem(key);
    },
    clear: () => {
        localStorage.clear();
    },
};

export default $storage;