import {MockUtil} from "../../mock/mock.js";
import {AuthUtil} from "../../util/auth.js";
import {DynamicRouteUtil} from "../../util/dynamicRoutes.js";
import $storage from "../../util/localStorage.js";

/**
 * 定义状态变量
 */
const state = {
    tokenValue: '',
    // 从 localStorage 里面获取的菜单列表
    menuList: $storage.getJson('menuList') || [],
};

/**
 * 定义修改状态的同步方法
 * 通过 commit 调用 mutations 定义的方法
 */
const mutations = {
    SET_TOKEN_VALUE(state, value) {
        // 修改 state 变量的值
        state.tokenValue = value;
    },
    UPDATE_USER_MENU_LIST(state, menuList) {
        // 取反：原来如果是折叠状态，那么就改为展开状态，反之亦然
        state.menuList = menuList;
        // 保存到 localStorage 里面
        $storage.setJson('menuList', menuList);
    }
};

/**
 * 定义修改状态的同步方法
 * 通过 dispatch 调用 actions 定义的方法
 */
const actions = {
    updateTokenValue({commit}, tokenValue) {
        // 通过 mutations 去更新 tokenValue 的值
        commit('SET_TOKEN_VALUE', tokenValue);
    },
    // 调用后端的登录接口
    handleLogin({commit}, request) {
        return new Promise((resolve, reject) => {
            // TODO 这里调用后端的登录接口，获取登录成功的 token 信息
            // 登录成功,保存token到本地缓存
            $storage.set('token', request.username);
            // 这里模拟登录成功的数据
            return resolve({
                code: 200,
                message: '登录成功',
                data: {
                    token: 'token'
                }
            });
        });
    },
    // 调用后端接口，完成用户注销登录
    handleLogout({commit}) {
        // TODO 这里调用后端的注销接口
        // 注销成功,清空本地缓存
        $storage.clear();
    },
    /**
     * 调用后端接口，获取用户菜单列表
     */
    getUserMenuList({commit}) {
        return new Promise((resolve, reject) => {
            // todo 这里可以调用后端接口，获取用户菜单列表
            // todo 模拟获取到的接口数据
            // todo 这里到时候对接后端的时候，只需要打开注释内容，修改getMenuList()方法即可
            // getUserMenu().then((res) => {
            // const menuList = res.data;
            // todo 假设数据获取成功
            const menuList = MockUtil.getMenuData(AuthUtil.getToken());
            // 将后端返回的菜单数据，解析成 VueRouter 中的路由格式
            const routeMenuList = DynamicRouteUtil.parseRouteMenu(menuList);
            // 保存到 localStorage 里面
            commit('UPDATE_USER_MENU_LIST', routeMenuList);
            // 将获取到的菜单数据返回
            resolve(routeMenuList);
            // }).catch((err) => {
            //     console.log('获取用户菜单失败:err=', err);
            //     reject(err);
            // });
        });
    },
};

// 导出相关的对象
export default {
    namespaced: true,
    state,
    mutations,
    actions
};