import {createRouter, createWebHistory} from 'vue-router';

// 定义静态路由信息
const routes = [
    {
        path: '/',
        redirect: '/main'
    },
    {
        path: '/main',
        name: 'main',
        meta: {
            title: '管理后台'
        },
        component: () => import('../pages/main/index.vue')
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '登录注册'
        },
        component: () => import('../pages/login/index03.vue'),
    },
    // 页面找不到的时候，会被这个路由进行处理
    {
        path: '/:pathMatch(.*)*',
        name: '404',
        component: () => import('../pages/error/404.vue'),
    },
    // {
    //     path: '/index',
    //     component: () => import('../pages/index.vue'),
    // },
    // {
    //     path: '/cropper',
    //     component: () => import('../pages/image/image-cropper-demo.vue'),
    // },
    // {
    //     path: '/icon',
    //     component: () => import('../pages/icon/IconDemo.vue')
    // },
    // {
    //     path: '/font-awesome/icon',
    //     component: () => import('../pages/icon/FontAwesomeDemo.vue')
    // },
    // {
    //     path: '/svg/icon',
    //     component: () => import('../pages/icon/SvgDemo.vue')
    // },
    // {
    //     path: '/main',
    //     name: 'main',
    //     component: () => import('../pages/main/index.vue'),
    //     children: [
    //         {
    //             path: '/demo/pageOne',
    //             name: 'pageOne',
    //             component: () => import('../pages/demo/PageA.vue'),
    //         },
    //         {
    //             path: '/demo/pageTwo',
    //             name: 'pageTwo',
    //             component: () => import('../pages/demo/PageB.vue'),
    //         },
    //     ]
    // },
];

// 创建路由对象
const router = createRouter({
    history: createWebHistory(), routes,
});

// 导出路由对象
export default router;